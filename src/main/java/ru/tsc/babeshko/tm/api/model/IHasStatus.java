package ru.tsc.babeshko.tm.api.model;

import ru.tsc.babeshko.tm.enumerated.Status;

public interface IHasStatus {

    Status getStatus();

    void setStatus(Status status);

}
